﻿using ComputerWorld.DB;
using ComputerWorld.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ComputerWorld.Pages
{
    /// <summary>
    /// Interaction logic for AddServicePage.xaml
    /// </summary>
    public partial class AddServicePage : Page
    {
        public AddServicePage()
        {
            InitializeComponent();
        }

        private void backBtn_Click(object sender, RoutedEventArgs e)
        {
            PageHelper.MainFrame.GoBack();
        }

        private void addBtn_Click(object sender, RoutedEventArgs e)
        {
            Service service = new Service();
            service.ServiceName = tbService.Text;
            service.Cost = Convert.ToDecimal(tbService.Text);
            service.ServiceType = tbServiceType.Text;

            if (MessageBoxResult.Yes == MessageBox.Show("Вы действительно хотите добавить запись?","Предупреждение", MessageBoxButton.YesNo))
            {
                PageHelper.DbConnect.Service.Add(service);
                PageHelper.DbConnect.SaveChangesAsync();
            }
        }
    }
}
