﻿using ComputerWorld.DB;
using ComputerWorld.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ComputerWorld.Pages
{
    /// <summary>
    /// Interaction logic for EditServicePage.xaml
    /// </summary>
    public partial class EditServicePage : Page
    {
        Service _service;

        public EditServicePage(Service service)
        {
            InitializeComponent();

            _service = service;
            DataContext = _service;
        }

        private void backBtn_Click(object sender, RoutedEventArgs e)
        {
            PageHelper.MainFrame.GoBack();
        }

        private void editBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBoxResult.Yes == MessageBox.Show("Вы действительно хотите изменить запись?", "Предупреждение", MessageBoxButton.YesNo))
                {
                    PageHelper.DbConnect.SaveChangesAsync();
                    MessageBox.Show("Данные сохранены");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
