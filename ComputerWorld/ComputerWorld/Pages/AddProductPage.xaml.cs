﻿using ComputerWorld.DB;
using ComputerWorld.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ComputerWorld.Pages
{
    /// <summary>
    /// Interaction logic for AddProductPage.xaml
    /// </summary>
    public partial class AddProductPage : Page
    {
        public AddProductPage()
        {
            InitializeComponent();
        }

        private void backBtn_Click(object sender, RoutedEventArgs e)
        {
            PageHelper.MainFrame.GoBack();
        }

        private void addBtn_Click(object sender, RoutedEventArgs e)
        {
            Product product = new Product();
            product.ProductType = tbProductType.Text;
            product.ProductName = tbProductName.Text;
            product.ProductManufacturer = tbProductManufacturer.Text;
            product.ProductCost = Convert.ToDecimal(tbProductCost.Text);
            product.ProductDescription = tbProductDescription.Text;

            if (MessageBoxResult.Yes == MessageBox.Show("Вы действительно хотите добавить запись?", "Предупреждение", MessageBoxButton.YesNo))
            {
                PageHelper.DbConnect.Product.Add(product);
                PageHelper.DbConnect.SaveChangesAsync();
            }
        }
    }
}
