﻿using ComputerWorld.DB;
using ComputerWorld.Helpers;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ComputerWorld.Pages
{
    /// <summary>
    /// Interaction logic for ServicePage.xaml
    /// </summary>
    public partial class ServicePage : Page
    {
        DbSet<Service> service;

        public ServicePage()
        {
            InitializeComponent();

            PageHelper.PageName.Text = "Услуги сервисного центра";

            UpdateList();
        }

        private void UpdateList()
        {
            service = PageHelper.DbConnect.Service;
            lvService.ItemsSource = service.ToList();
        }

        private void backBtn_Click(object sender, RoutedEventArgs e)
        {
            PageHelper.MainFrame.GoBack();
        }

        private void addBtn_Click(object sender, RoutedEventArgs e)
        {
            PageHelper.MainFrame.Navigate(new AddServicePage());
        }

        private void editBtn_Click(object sender, RoutedEventArgs e)
        {
            var selected = lvService.SelectedItems as Service;
            if (selected != null)
            {
                PageHelper.MainFrame.Navigate(new EditServicePage(selected));
            }
            else
            {
                MessageBox.Show("Нет выбранной записи");
            }
        }

        private void deleteBtn_Click(object sender, RoutedEventArgs e)
        {
            var selected = lvService.SelectedItems as Service;
            if (selected != null)
            {
                if (MessageBoxResult.Yes == MessageBox.Show("Вы действительно хотите удалить запись?","Предупреждение", MessageBoxButton.YesNo))
                {
                    PageHelper.DbConnect.Service.Remove(selected);
                    PageHelper.DbConnect.SaveChanges();
                    UpdateList();
                }
            }
            else
            {
                MessageBox.Show("Нет выбранной записи");
            }
        }
    }
}
