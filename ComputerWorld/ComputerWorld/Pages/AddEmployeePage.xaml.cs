﻿using ComputerWorld.DB;
using ComputerWorld.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ComputerWorld.Pages
{
    /// <summary>
    /// Interaction logic for AddEmployeePage.xaml
    /// </summary>
    public partial class AddEmployeePage : Page
    {
        public AddEmployeePage()
        {
            InitializeComponent();
        }

        private void backBtn_Click(object sender, RoutedEventArgs e)
        {
            PageHelper.MainFrame.GoBack();
        }

        private void addBtn_Click(object sender, RoutedEventArgs e)
        {
            Employee employee = new Employee();
            employee.FirstName = tbFirstName.Text;
            employee.MiddleName = tbMiddleName.Text;
            employee.SecondName = tbSecondName.Text;
            employee.TelephoneNumber = tbTelephoneNumber.Text;
            employee.Email = tbEmail.Text;
            employee.Address = tbAddress.Text;
            employee.PassportSeries = tbPassportSeries.Text;
            employee.PassportNumber = tbPassportNumber.Text;
            employee.RoleName = tbRoleName.Text;

            if (MessageBoxResult.Yes == MessageBox.Show("Вы действительно хотите добавить запись?", "Предупреждение", MessageBoxButton.YesNo))
            {
                PageHelper.DbConnect.Employee.Add(employee);
                PageHelper.DbConnect.SaveChangesAsync();
            }
        }
    }
}
