﻿using ComputerWorld.DB;
using ComputerWorld.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ComputerWorld.Pages
{
    /// <summary>
    /// Interaction logic for EditEmployeePage.xaml
    /// </summary>
    public partial class EditEmployeePage : Page
    {
        Employee _employee;

        public EditEmployeePage(Employee employee)
        {
            InitializeComponent();

            _employee = employee;
            DataContext = _employee;
        }

        private void backBtn_Click(object sender, RoutedEventArgs e)
        {
            PageHelper.MainFrame.GoBack();
        }

        private void addBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBoxResult.Yes == MessageBox.Show("Вы действительно хотите изменить запись?", "Предупреждение", MessageBoxButton.YesNo))
                {
                    PageHelper.DbConnect.SaveChangesAsync();
                    MessageBox.Show("Данные сохранены");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
