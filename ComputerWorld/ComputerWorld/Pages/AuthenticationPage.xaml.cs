﻿using ComputerWorld.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ComputerWorld.Pages
{
    /// <summary>
    /// Interaction logic for AuthenticationPage.xaml
    /// </summary>
    public partial class AuthenticationPage : Page
    {
        public AuthenticationPage()
        {
            InitializeComponent();

            PageHelper.PageName.Text = "Авторизация";
        }

        private void enterBtn_Click(object sender, RoutedEventArgs e)
        {
            if (PageHelper.DbConnect.User.Where(x => x.Login == tbLogin.Text && x.Password == tbPassword.Password).FirstOrDefault() == null)
            {
                MessageBox.Show("Неверные данные", "Ошибка авторизации");
                return;
            }
            if (PageHelper.DbConnect.User.Where(x => x.Login == tbLogin.Text && x.Password == tbPassword.Password && x.RoleID == 1).FirstOrDefault() != null)
            {
                PageHelper.role = 1;
                PageHelper.MainFrame.Navigate(new Pages.MainPage());
            }
            if (PageHelper.DbConnect.User.Where(x => x.Login == tbLogin.Text && x.Password == tbPassword.Password && x.RoleID == 2).FirstOrDefault() != null)
            {
                PageHelper.role = 2;
                PageHelper.MainFrame.Navigate(new Pages.MainPage());
            }
        }
    }
}
